const openNav = document.querySelector('.header');
const headerNav = document.querySelector('.header__nav');

openNav.addEventListener('click', (e) => {
  if (window.innerWidth < 960) {
    if (headerNav.classList.contains('isShow')) {
      headerNav.className = ('header__nav');
    } else {
      headerNav.className = ('header__nav isShow');
    }
  }
});
